---
title: |
 | Metodi e tecniche della ricerca: uso del Software R 
 | Esercizi parte 5
author : "Fabio Morea"
date: "01/12/2022"
output : pdf_document
---
# Introduzione

L'obiettivo del quarto gruppo di esercizi è presentare le funzionalità della libreria tidyverse

# Esercizio 5.1
```{r include=FALSE}
library(tidyverse)
library(lubridate)
```

```{r}
url_dati_settimanali <- "https://dgsaie.mise.gov.it/open_data_export.php?export-id=4&export-type=csv"

dati_sett_completi <- read_delim(url_dati_settimanali, delim = ",")

#str(dati_sett_completi)

dati_sett <- dati_sett_completi %>%
    select(DATA_RILEVAZIONE, NOME_PRODOTTO, PREZZO ) %>%
    filter( NOME_PRODOTTO %in% c("Benzina", "Gasolio auto")) %>%
    mutate(anno = year(DATA_RILEVAZIONE))   

 

#str(dati_sett)

plot( dati_sett$PREZZO, 
      type = "l", 
      col = "red", 
      main = "Prezzi settimanali di benzina e gasolio")
```


```{r}
benzina  <- dati_sett %>% filter( NOME_PRODOTTO  == "Benzina")  
gasolio  <- dati_sett %>% filter( NOME_PRODOTTO  == "Gasolio auto") 

plot(benzina$PREZZO, type = "l", col = "red")
lines(gasolio$PREZZO, , col = "blue")
```

```{r}
dati_grafico <- dati_sett %>%
    rename(data_della_rilevazione = DATA_RILEVAZIONE) %>%
    rename(prezzo = PREZZO) %>%
    rename(prodotto = NOME_PRODOTTO) %>%
    filter(anno == 2022) %>%
    mutate(prezzo = prezzo / 1000)
    

dati_grafico %>% 
    ggplot() + 
    aes(x = data_della_rilevazione,  y = prezzo, color = prodotto ) +
    geom_line() + 
    geom_point(shape=22, size = 1) +
    scale_colour_manual(values = c("red","blue"))+
    ggtitle("Prezzi dei carburanti [€ al litro]") + 
    theme_light()

ggsave("figura1.png", width = 30,  height = 15,  units = "cm")

#ggsave("./risultati/figura2.png", width = 12, height = 12, units = "cm")


```



```{r}
url_dati_completi <- "https://dgsaie.mise.gov.it/open_data_export.php?export-id=11&export-type=csvx"
dati_completi <- read_delim(url_dati_completi, delim = ";")
spec(dati_completi)
dati <- dati_completi %>%
    select(-IVA, -ACCISA, -NETTO ) %>%
    filter( NOME_PRODOTTO %in% c("Benzina", "Gasolio auto")) 


```

# download file and save it to local folder

```{r}
#url_dati <- "https://dgsaie.mise.gov.it/open_data_export.php?export-id=4&export-type=csvx" 
#print(paste("Download dei dati dall'indirizzo", url_dati))
#nome_file <- "./dati/dati.csv"
#download.file(url_dati)
#read.csv(file = nome_file, sep = ";")

```



```{r}
hist(dati_completi$PREZZO )
```

