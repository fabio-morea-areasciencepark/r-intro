---
title: "Variazione del prezzo della benzina"
author : "Fabio Morea"
date: "16/12/2022"
output : pdf_document
---

# Obiettivo
Questo documento presenta sinteticamente la  variazione del prezzo della benzina in Italia tra gennaio e settembre 2022, utilizzando i dati disponibili nel portale del Ministero dell'ambiente <https://dgsaie.mise.gov.it/prezzi_carburanti_mensili.php>.

# Risultato

```{r eval=FALSE, include=FALSE}
# NOTA: tutti i blocchi di codice di questo file sono nascosti, 
# quindi non compaiono nella versione .PDF
# ma sono utili per documentare le operazioni eseguite
```


```{r include=FALSE}
# Dati: prezzi sono  in Euro per 1000 litri di benzina.
prezzo_iniziale <- 1764.74 
prezzo_finale <- 1678.97
```


```{r include=FALSE}
#calcolo dei prezzi al litro
prezzo_litro_iniziale <- prezzo_iniziale / 1000
prezzo_litro_finale   <- prezzo_finale   / 1000

# la funzione round() arrotonda il valore ad un dato numero di cifre decimali
variazione_litro <- round( prezzo_litro_finale - prezzo_litro_iniziale, digits = 2)
```


```{r include=FALSE}
if (variazione_litro == 0) {
	tipo_variazione = "invariato"
}else{
	if (variazione_litro > 0 ) {
		tipo_variazione = "aumentato"
	}else{
		tipo_variazione = "diminuito"
	}
}
```


```{r warning=TRUE, include=FALSE}
# calcolo della variazione percentuale
variazione_percentuale <- round( variazione_litro / prezzo_litro_iniziale, digits = 4) * 100
```

Tra gennaio e ottobre 2022 il prezzo della benzina è `r tipo_variazione`, passando da `r round(prezzo_litro_iniziale, digits = 4)` € al litro a `r round(prezzo_litro_finale, digits = 4)` € al litro con una variazione del `r variazione_percentuale` %.

 