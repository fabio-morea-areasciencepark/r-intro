# R-intro
A short introduction to R programming language, aimed at researchers that are currently using spreadsheets.

## Language
Italian

## Author
Fabio Morea, Area Science Park (www.areasciencepark.it)

## Project
A short introduction to R programming language, aimed at researchers that are currently using spreadsheets.

I seminari offrono una rapida introduzione al linguaggio di programmazione R finalizzato all’analisi di dati in formato tabellare e alla produzione di visualizzazioni grafiche. 
Obiettivi:
-   Capire le potenzialità del linguaggio di programmazione **R** per analizzare dati in formato tabellare, produrre grafici e documentare le procedure di calcolo utilizzate
-   Iniziare ad utilizzare il linguaggio **R** e l'ambiente di sviluppo **RStudio**
-   Costruire una prima collezione di semplici script da riutilizzare
-   Acquisire la motivazione e le informazioni necessarie per continuare ad imparare

## Updates
16/12/2022


 



